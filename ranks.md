# Fleet Ranks
- Ranks are gained mainly by social activity like chatting, joining events and TFO

## Assimilated
- Base rank

## Upgraded Assimilated
- Gained upon observed several times chatting in Fleet/Armada chat.

## Fully Assimilated
- Gained from joining Fleet Events if the person is already `Upgraded Assimilated`
- Otherwise he'll get `Upgraded Assimilated` first instead

## Singularity
- Junior Officer Rank
- Given to people who do useful stuff for the fleet

## Anomaly
- Senior Officer Rank
- Given to well-performing Officers

## Balance
- Inner Circle Rank
- Given to those who are exceptionally trusted

## Synergy
- Leader rank
