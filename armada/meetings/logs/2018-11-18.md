# Armada meeting log

## Attendees
### P-FLEET
  - Pork@Bad2Born
  - Mheero@mindwind
  - Mtn Dew@incredablehefey
  - Vivek@litchyakuza#4850
  - Vidak'Tar@SoulSpector
### 16th AAB
  - Tyler@Adm Chambers
  - Brea@Chuaos
  - scientia@kingzool#6421
  - Kelera Kriss@domonis
  - Texadasa@hall148
### Assimilated Synergy
  - Ragara@ziktofel
  - Sienna@andy#5498
### UFP
  - Kudman'Ralan'fTw@Hansak
### Raven Squad
  - W1ld0n3@thgwildone
  - Kaure@geostaryn#4095
### Archangels
  - fiorello netherlands@maffioso2012

## New fleets
- The Archangels (gamma of Assimilated)
- @maffioso2012 and his wife should focus their efforts on that fleet now

## Joint Armada Task Force
- Such events together with others fleets could happen once per month

## STO career events
- Should happen once per week
- Should last 1 hour
- No prize items

## Joining "Super-Armada"
- P-FLEET armada won't join them now
- Miscommunication was cleared up
- Individual fleets within our armada are allowed to advertise and to join their discord
- Assimilated Synergy will try that at first

## Event prizes
- All fleets should create a prize section within fleetbank for use during special events
- 16th AAB and Assimilated Synergy already have this
