# Fleet dashboard
## News and Info
- Fleet site: https://gitlab.com/assimilated-synergy/public
- Focus on Dilmine and holding upgrades!
- Last Fleet Meeting: [fleet-meetings/logs/2019-01-12.md](fleet-meetings/logs/2019-01-12.md)
- Last Armada Meeting: [armada/meetings/logs/2018-12-22.md](armada/meetings/logs/2018-12-22.md)
- Use the armada chat for non-fleet-internal stuff (STFs gathering and general chat)
- We are looking for Officers. Contact Sienna@andy#5498 for more info!

## Pork's Wheel Of Misfortune
- We're participating in the Wheel Of Misfortune game together with P-FLEET and 16th AAB
- How to participate: 
    - Deposit 1M EC into Fleet Bank. That's all
    - Up to two entries per Account per draw
    - Participation for each draw is closed every Sunday. Whenever it happens it gets posted here
- Winner is drawn every week on Monday, check Armada discord
    - 10% of entries goes into the Fleet for Event Items and other stuff. The rest goes into Prize Pool!
    - Winner is determined by using this app: https://wheeldecide.com/
    - The wheel contains every ticket + two special options:
        - Bonus 10M into prize pool + reroll
        - All tickets postponing into next round. If that happens the prize pool and all tickets are added into next draw but no one wins
- 3rd round closed. Accepting contributions to 4th round. Registered for last closed round:
    - participants: [lotto/participants](lotto/participants)
    - prize pool (from our fleet): [lotto/prize-pool-amount](lotto/prize-pool-amount)   

## Current events
- Colony Invasion Fleet Event
    - Gather your best weapons, and strap on your best armour as its time to go and beat the invasion thats attacking the colony, team up with your fellow fleet mates, and show these invaders that this colony will not fall! Qapla
    - Time: 2018-12-15 17:00 UTC Local time: https://www.timeanddate.com/worldclock/fixedtime.html?msg=Colony+Invasion+Fleet+Event&iso=20181215T17&p1=1440
    - Event Owner: R'Tech@weonline#2321
- Fleet PvP
    - Time to get together with your fellow fleeties, for some good ole blowing each other up, come and join us and test your other fleet mates builds, get to know your fleet more! and just some good fun and games 
    - Time: 2018-12-02 17:00 UTC Local time: https://www.timeanddate.com/worldclock/fixedtime.html?msg=Fleet+PvP&iso=20181202T17&p1=1440
    - Event Owner: R'Tech@weonline#2321
- Fiorello's PVP Battle
    - Armada-wide event
    - Fiorello has decreed the the fleets of the armada will battle valianetly and with nobility for the lords amusement the most skilled of these contenders will recevie the following rewards:
    - Prize pool:
        - 1st prize lockbox key sponsord by maffioso wife ingame name @ndaanje86#5696
        - 2nd prize epic TR-116B Rifle - Sniper Rifle Mk XV [4xShPen] [CrtH]x2 [Dm/CrH] [Dmg] @maffioso2012
        - 3rd prize mirror ship sponsord by the 16th fleet
    - Event Owner: fiorello netherlands@maffioso2012
- Fleet Meeting
    - Come to discuss our current fleet affairs! Everyone is welcome!
    - Time: 2018-12-08 21:00 UTC Local time: https://www.timeanddate.com/worldclock/fixedtime.html?msg=Fleet+Meeting&iso=20181208T21&p1=1440
    - Event Owner: Ragara@ziktofel
- Saturday Warzone
    - Just for fun Armada-wide event to invade warzones in the space or at the ground
    - Time: 2018-12-08 19:00 UTC Local time: https://www.timeanddate.com/worldclock/fixedtime.html?msg=Saturday+Warzone&iso=20181208T1900&p1=1440
    - Event Owner: Pork@Bad2Born
- Armada Meeting
    - Come to discuss our current armada affairs! Everyone is welcome!
    - Time: TBD
    - Event Owner: Pork@Bad2Born
- Fleet PvE
    - Come and join us in some advance pve cues (TFO'S) lets show the enemy what we at Assimilated Synergy are made of, group up with fellow fleeties and have some good chats and fun! See you on the horizon Captain!
    - Time: 2018-12-08 18:20 UTC Local time: https://www.timeanddate.com/worldclock/fixedtime.html?msg=Fleet+PvE&iso=20181208T1820&p1=1440
    - Event Owner: R'Tech@weonline#2321

## Links
- Tips and Tricks (16th AAB's Knowledge Base): https://gitlab.com/16th-Air-Assault-Brigade/public/tree/master/knowledge-base
- Fleet and Armada Discord: https://discord.gg/M7WqtPR

## Roles and responsibilites
### Fleet Leader
- Sienna@andy#5498 (Gitlab account @Aleyna_Scott)
- Ragara@ziktofel (Gitlab account @Ziktofel)

### Advisor Team
- Alt@Bad2Born
- KeVekle@WoLF_KABOOM (Gitlab account @WoLFKABOOM)
- Larine@Chuaos (Gitlab account @Chuaos)

### Fleet Officers
#### Fleet Events
- Coralia@kaboose008
- Freya@empusathedark
#### Discord
- R'Tech@weonline#2321
#### Recruiting
- James Connor@djeriksen98
#### Special Duties
- Jem'Hodor@Captain_Nicholls21
- Vanessa Murakuma@ghostwalker#1553
#### Fleet Bank
- Jason@ibok92

